package topn

import "testing"

func BenchmarkSingle(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := Single(testFile, 10)
		if err != nil {
			panic(err)
		}
	}
}

func TestTopNSingle(t *testing.T) {

	results, err := Single(testFile, 10)
	if err != nil {
		panic(err)
	}
	t.Log(results)
}
