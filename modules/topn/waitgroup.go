package topn

import (
	"bufio"
	"io"
	"math"
	"os"
	"runtime"
	"sort"
	"strconv"
	"sync"
)

//SectionFile represent each block for read
type SectionFile struct {
	Offset int64
	N      int64
}

func calculateBreakOffset(file *os.File, offset int64) int64 {

	file.Seek(offset, 0)
	reader := bufio.NewReader(file)
	bytes, err := reader.ReadBytes('\n')
	if err != nil {
		return 0
	}

	return int64(len(bytes)) - int64(2)

}

//WaitGroup using wait groups for best performance
func WaitGroup(file string, n int) ([]int, error) {
	results := make([]int, n)

	inputFile, err := os.Open(file)
	if err != nil {
		return results, err
	}
	defer inputFile.Close()
	info, err := inputFile.Stat()
	if err != nil {
		return results, err
	}
	size := info.Size()

	numWorkers := runtime.NumCPU()
	sections := make([]SectionFile, numWorkers)
	divider := int64(math.Floor(float64(info.Size()) / float64(numWorkers)))
	offset := int64(0)
	for i := range sections {
		isLast := i == (numWorkers - 1)
		n := divider
		if isLast {
			n = size - offset
		}
		lineBreakOffset := calculateBreakOffset(inputFile, offset+n)
		n += lineBreakOffset
		sections[i] = SectionFile{
			N:      n,
			Offset: offset,
		}
		offset += n
	}

	_, err = inputFile.Seek(0, 0)
	if err != nil {
		return results, err
	}

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	for _, section := range sections {

		go func(s SectionFile) {
			defer wg.Done()

			sectionReader := io.NewSectionReader(inputFile, s.Offset, s.N)
			reader := bufio.NewReader(sectionReader)
			for {
				buffer, err := reader.ReadBytes('\n')
				if err != nil && err != io.EOF {
					return
				}
				totalBytes := len(buffer)
				if totalBytes == 0 {
					break
				}

				line := string(buffer[:totalBytes-1])
				if line == "" {
					continue
				}
				number, err := strconv.Atoi(line)
				if err != nil {
					return
				}

				if number > results[0] {
					results[0] = number
					sort.Ints(results)
				}

			}
		}(section)
	}
	wg.Wait()

	return results, nil
}
