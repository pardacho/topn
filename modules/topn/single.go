package topn

import (
	"bufio"
	"io"
	"os"
	"sort"
	"strconv"
)

//Single single example
func Single(file string, n int) ([]int, error) {
	results := make([]int, n)
	inputFile, err := os.Open(file)
	if err != nil {
		return results, err
	}
	defer inputFile.Close()

	inputFileReader := bufio.NewReader(inputFile)
	for {
		buffer, err := inputFileReader.ReadBytes('\n')
		if err != nil && err != io.EOF {
			return results, err
		}
		totalBytes := len(buffer)
		if totalBytes == 0 {
			break
		}

		line := string(buffer[:totalBytes-1])
		if line == "" {
			continue
		}
		number, err := strconv.Atoi(line)
		if err != nil {
			return results, err
		}

		if number > results[0] {
			results[0] = number
			sort.Ints(results)
		}

	}

	return results, nil
}
