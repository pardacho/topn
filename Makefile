REGISTRY=registry.gitlab.com/pardacho/topn
VERSION=1.0
BUILD=go build -ldflags="-w -s"

default: build

build: format lint
	$(BUILD) -o topn main.go
	upx topn

deps:
	glide install

test:
	go test $$(go list ./... | grep -v /vendor/)

format:
	go fmt $$(go list ./... | grep -v /vendor/)

lint:
	golint -set_exit_status -min_confidence 0.3 $$(go list ./... | grep -v /vendor/)

registry: registry-build registry-push registry-clear

registry-build:
	docker build --pull -f docker/go/Dockerfile-prod -t $(REGISTRY):$(VERSION) .

registry-pull:
	docker pull $(REGISTRY):$(VERSION)

registry-push:
	docker push $(REGISTRY):$(VERSION)

registry-clear:
	docker image rm -f $(REGISTRY):$(VERSION)

stop:
	docker-compose stop

stop-prod:
	docker-compose -f docker-compose-prod.yml stop

dev:
	docker-compose build
	docker-compose up -d
	clear
	@echo ""
	@echo "starting command line:"
	@echo "** when finish exist and run: make stop**"
	@echo ""
	docker-compose exec server sh

prod:
	docker-compose -f docker-compose-prod.yml build
	clear
	@echo ""
	@echo "usage example:"
	@echo "./topn.sh file.txt"
	@echo ""

