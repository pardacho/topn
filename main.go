package main

import (
	"flag"
	"fmt"
	"gitlab.com/pardacho/topn/modules/topn"
	"os"
	"runtime"
	"runtime/pprof"
	"strings"
)

var useWaitGroup bool
var cpuProfile string
var memProfile string
var n int

func main() {

	flag.StringVar(&cpuProfile, "cpuprofile", "", "write cpu profile to file")
	flag.StringVar(&memProfile, "memprofile", "", "write memory profile to file")
	flag.BoolVar(&useWaitGroup, "waitgroup", true, "use waitgroup")
	flag.IntVar(&n, "n", 10, "N for top")

	flag.Parse()

	var err error
	var results []int
	fileName := strings.TrimSpace(os.Args[1])
	if fileName == "" {
		panic("must require filename")
	}

	if cpuProfile != "" {
		f, err := os.Create(cpuProfile)
		if err != nil {
			panic(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if !useWaitGroup {
		results, err = topn.Single(fileName, n)
	} else {
		fmt.Println("using waitgroup...")
		results, err = topn.WaitGroup(fileName, n)
	}

	fmt.Println(results)

	if memProfile != "" {
		f, err := os.Create(memProfile)
		if err != nil {
			panic(err)
		}
		runtime.GC()
		if err := pprof.WriteHeapProfile(f); err != nil {
			panic(err)
		}
		f.Close()
	}

	if err != nil {
		panic(err)
	}

}
